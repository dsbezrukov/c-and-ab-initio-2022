#pragma once

/* Базисная библиотека хранит внутри себя базис
 * для каждого типа атома, который мы читаем из, например,data/631+g*.txt.
 * По факту каждому атому, который определяется int (его зарядом)
 * ставится в соответствие несколько функций, которые определяются
 * своей оболочкой (s,p,d) и радиальной частью \sum_i C_i exp (-a_i * r^2 ).
 * Этот фаакт и отражен в поле класс content.
*/

#include <string>
#include <map>
#include <vector>

struct oneTerm_t {
    double a{0};
    double c{0};
};

using radial_t = std::vector<oneTerm_t>;

class BasisLib {
public:
    const char* loadFromGamessUS(std::string filename);
    static int getT(const std::string& name);
    //    const char* saveGamessUS_basis(std::string filename);

public:
    std::map<int, std::vector<std::vector<radial_t> > >
        content{};  // номер оболочки это индекс в векторе

private:
    const int L_MAX{10};
    static inline std::map<std::string, int> dict = {
        {"L", -1}, {"S", 0}, {"P", 1}, {"D", 2}};
};
