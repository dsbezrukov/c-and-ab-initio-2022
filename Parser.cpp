#include "Parser.h"
#include <iostream>

Parser::Parser(const int argc, char *argv[]) {
    for (int i = 1; i < argc; i++) {
        auto key = std::string(argv[i]);

        if ((key == "--geom") or (key == "-g")) {
            if (++i == argc) {
                std::cerr
                    << "Error! Expected value after key -geom is empty!\n";
                return;
            }
            xyzfile = std::string(argv[i]);
            continue;
        }
        if ((key == "--basis") or (key == "-b")) {
            if (++i == argc) {
                std::cerr
                    << "Error! Expected value after key -basis is empty!\n";
                return;
            }
            basisfile = std::string(argv[i]);
            continue;
        }
        if ((key == "--charge") or (key == "-c")) {
            if (++i == argc) {
                std::cerr
                    << "Error! Expected value after key -charge is empty!\n";
                return;
            }
            charge = atoi(argv[i]);
            continue;
        }
        std::cerr << "Error! Unknown key \"" + key + "\". \n";
        return;
    }
}

std::string Parser::getXyzFile() const { return xyzfile; }

std::string Parser::getBasisFile() const { return basisfile; }

int Parser::getChare() const { return charge; }

const char *Parser::isGood() const {
    if (xyzfile.empty()) return "XYZ file not specified";
    if (basisfile.empty()) return "Basis file not specified";
    return nullptr;
}
