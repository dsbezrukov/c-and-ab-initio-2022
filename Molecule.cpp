#include "Molecule.h"
#include <fstream>

const char* Molecule::loadFromXyz(std::string filename) {
    std::ifstream inp(filename.c_str());
    std::string tStr;
    Atom tAtom;
    std::getline(inp, tStr);
    std::getline(inp, tStr);
    while (inp >> tStr >> tAtom.x >> tAtom.y >> tAtom.z) {
        tAtom.q = getQ(tStr);
        if (tAtom.q > 0) {
            atoms.push_back(tAtom);
        } else {
            return ("Unknown atom name " + tStr).c_str();
        }
    }
    inp.close();
    return nullptr;
}

int Molecule::getQ(const std::string name) {
    std::string upperName(name);
    for (auto& c : upperName) c = toupper(c);
    if (dict.find(upperName) == dict.end()) return -1;
    return dict[upperName];
}
