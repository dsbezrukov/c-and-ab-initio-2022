#pragma once

#include <string>
#include <vector>
#include <map>

struct Atom {
    double x{0}, y{0}, z{0}, q{0};
};

class Molecule {
public:
    std::vector<Atom> atoms{};
    const char* loadFromXyz(const std::string filename);
    static int getQ(const std::string name);

private:
    static inline std::map<const std::string, int> dict = {
        {"H", 1},         {"HYDROGEN", 1}, {"O", 8},      {"OXYGEN", 8},
        {"N", 7},         {"HYDROGEN", 1}, {"HELIUM", 2}, {"LITHIUM", 3},
        {"BERYLLIUM", 4}, {"BORON", 5},    {"CARBON", 6}, {"NITROGEN", 7},
        {"OXYGEN", 8},    {"FLUORINE", 9}, {"NEON", 10}};
};
